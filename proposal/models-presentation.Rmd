---
title: "模型理论及模拟"
author: "SXM"
date: "2019年4月"
institute: 中南财经政法大学统计与数学学院
csl: ./style/chinese-gb7714-2015-numeric.csl
css: ./style/markdown.css
bibliography: [./Bibfile.bib]
eqnPrefixTemplate: ($$i$$)
link-citations: true
linkReferences: true
chapters: true
tableEqns: false
autoEqnLabels: true
classoption: "aspectratio=1610"
---

```{r setup, echo=F, purl=F}
knitr::opts_knit$set(root.dir = getwd())
knitr::opts_chunk$set(echo = TRUE, results = 'hide')
knitr::opts_chunk$set(warning = FALSE, message=FALSE)
knitr::opts_chunk$set(fig.align="center"
                      ## ,out.width="0.9\\textwidth" # latex
                      ,out.width="60%" # for both latex and html
                      ,fig.width=5, fig.height=3
                      )
```

```{r prepare, echo=F, purl=F}
rm(list=ls())
options(digits=4)
options(scipen=100)
graphics.off()
Sys.setlocale("LC_ALL", "Chinese")
```

# 模型理论及模拟数据
## GARCH
### GARCH模型

Bollerslev(1986)[@Tim1986] 提出的GARCH模型(广义自回归条件异方差模型），很好地刻
画了金融资产收益率的异方差性和波动率的聚集效应。

GARCH(p,q)模型为
$$\left\{ \begin{array}{l}
{r_t} = \mu_t + {\varepsilon _t},{\varepsilon _t} = \sqrt {{h_t}} {z_t}\\
{h_t} = \omega  + \sum\limits_{i = 1}^p {{\beta _i}{h_{t - i}}}  + \sum\limits_{j = 1}^q {{\alpha _j}\varepsilon _{t - j}^2} 
\end{array} \right.$$
上述方程分别为条件均值方程和条件方差方程,式中，$r_t$是t时刻资产收益率,$\mu_t$的
确定性信息拟合模型,通常为常数且为0；$h_t$是t时刻条件方差，$z_t\sim i.i.d(0,1)$，
这个模型简记为GARCH(p,q).

___

估计资产波动率时，应用最为广泛的模型是GARCH模型。对于金融数据，GARCH(1,1)
模型足以刻画金融资产收益率的ARCH效应。

GARCH(1,1)模型为: 

$$\left\{ \begin{array}{l}
r_t=\mu_t+\varepsilon_t,
\varepsilon_t=\sqrt{h_t}z_t\\
h_t=\omega+\alpha\varepsilon_{t-1}^2+\beta h_{t-1}
\end{array} \right.$$

$\omega>0,\alpha>0,\beta>0$并且$\alpha+\beta<1$是方
程平稳性的充要条件。 

### 模拟GARCH(1,1)数据

```{r}
rm(list=ls())
set.seed(123)
n=1000
h=numeric(n) #条件方差
ep=numeric(n) #残差序列
r=numeric(n) #收益率
omega=0.2
alpha=0.2
beta=0.1
zt=rnorm(1000,0,1)
# 设置初始值
h[1]=(omega/(1-beta))
ep[1]=sqrt(h[1])*zt[1]
r[1]=0
for (t in 2:1000){
  h[t]=omega+beta*h[t-1]+alpha*(ep[t-1])^2#方差方程
  ep[t]=sqrt(h[t])*zt[t]
  r[t]=0.3*r[t-1]+ep[t] #均值方程
}
ts.plot(r)
```


## Realized GARCH
### Realized GARCH模型

Hansen等(2012) [@Hansen2012] 在GARCH模型基础上，用一个隐变量就可以实现收益率，波
动率和实现测度的联合建模.通过加入一个测量方程将 GARCH-X模型的条件波动率h和实现测
度X连接起来，形成一个封闭的完整模型，并在其中植入一个杠杆函数来描述 “信息冲击曲
线” [@王天一2012]。相比于其他利用高频
数据拟合波动率的模型，Realized GARCH模型不仅是一个封闭的完整模型，其计算方法也很
简便，不需要使用蒙特卡洛模拟，仅仅利用伪极大似然估计方法(QMLE)就可以求出参数值。
Hansen等(2012) [@Hansen2012]实证说明了对数形式的Realized GARCH模型比线性Realized
GARCH模型具有更好的实证效果(对数线性模型产生的异方差小($x_t$对$h_t$),而
异方差会使QMLE效率降低)。

对数形式的Realized GARCH(p,q)模型:
$$\begin{aligned}
&r_{t}=\sqrt{h_{t}} z_{t},z_t\sim N(0,1)\\
&\ln h_{t}=\omega+\sum_{i=1}^{p} \beta_{i} \ln h_{t-i}+\sum_{j=1}^{q} \gamma_{j} \ln x_{t-j}\\
&\ln x_{t}=\xi+\varphi \ln h_{t}+\tau\left(\mathrm{z}_{t}\right)+u_{t}, \quad
u_{t} \sim N \left(0, \sigma_{u}^{2}\right)
\end{aligned}$$

___

Realized GARCH(p,q)模型的三个等式分别为收益方程、GARCH方程和测量方程。$r_t$是金
融资产的日内收对数益率，前两个式子连接起来是一个标准的GARCH-X模型.$h_t=var(r_t|F_{t-1})$
表示条件方
差，也称为隐含波动率，$F_t=\sigma(r_t,x_t,r_{t-1},x_{t-1},\cdot)$。$x_t$表示
波动率的
已实现度量，使用RV($R V_{t}=\sum_{i=1}^{m} r_{t, i}^{2}$),
BV(双幂次变差$BV_{t}=\frac{\pi}{2} \frac{m}{m-1} \sum_{i=2}^{m}\left|r_{t,
i}\right|
 \cdot\left|r_{t, i-1}\right|$)等作为已实现波动率的估计。
测量方程将$h_t$和$x_t$联系起来。一般情况下假设标准差随机误差项$u_t \sim
N(0,\sigma_{u}^{2})$,且${z_t}$与${u_t}$相互独立，在Realized GARCH 模型中$h_t$
是$h_{t-1}$和$x_t$的函数。

为了描述收益率冲击对于波动率的非对称影响，这里在测量方程中加入了杠
杆函数 $\tau(z_t)$，
Hansen等(2012) [@Hansen2012] 采用了Hermite多项式构造的杠杆函数，并将$\tau(z_t)$设定为以下二次形式：
$$\tau(z_t)=\tau_1z_t+\tau_2(z_t^2-1)$$
其中，$E(\tau(z_t))=0$,$\tau_1$和$\tau_2$决定了波
动率对于正向收益率冲击和负向收益率冲击的不同反应，一般认为$\tau_1<0$和$\tau_2>0$。选择二次形式是可以确保对任何
$E(z_t)=0,var(z_t)=1$的分布有$E(\tau(z_t))=0$。具有二次杠杆的对数线性模型能够保
证$z_t$和$u_t$独立性的假定。杠杆函数描述了已实现 GARCH 模
型的信息冲击曲线，表明过去时刻的收益率在大小和方向上同时都影响着波动率，即正的价
格扰动对波动的影响和负的价格扰动对波动率产生的影响在大小和方向上都不一致的。

如果$RV_t$是实际波动率的无偏估计量，则测度方程中的参数
$\xi$和$\varphi$分别趋于 0 和 1，但是由于金融市场中存在微观白噪音问题，并且可能
发生的非交易时间等问题，可能实际波动率并不是求出的已实现波动率，因此存在误差,并
不约束为0和1。


___

Realized GARCH(1,1)模型：
$$r_{t}=\sqrt{h_{t}} z_{t},z_t\sim N(0,1)$$
$$\ln h_{t}=\omega+\beta \ln h_{t-1}+\gamma \ln x_{t-1}$$
$$\ln x_{t}=\xi+\varphi \ln h_{t}+\tau\left(\mathrm{z}_{t}\right)+u_{t}, \quad u_{t} \sim N  \left(0, \sigma_{u}^{2}\right)$$

$x_t$是基于高频数据计算的已实现测度。与前面两式的GARCH-X模型类似，已实现测度作为
更精确的信息源，可以提高波动率的建模和预测能力。式(7)被称为测量方程，是Realized
GARCH模型的核心设定。 其直观解释是条件方差$h_t$是已实现测度$x_t$的一个估计，由于
已实现测度计算仅能覆盖交易时间的波动，而条件方差覆盖的范围多了一个隔夜收益的波
动，因此系数$\varphi$并不约束为1。 

___

将式(6)带入式(7)，得到模型的简约形式(reduced form)
$$\ln h_{t+1}=(\omega+\xi \gamma)+(\beta+\varphi \gamma) \ln h_{t}+\gamma \tau\left(z_{t}\right)+\gamma u_{t}$$

在Realized GARCH模型中需要估计的参数有$\omega,\beta,\varphi,\gamma,\xi,\sigma_{u},\tau_1,\tau_2$

由于杠杆函数的设定保证了其均值为零，隐含波动率的滞后项系数
$\pi\equiv(\beta+\varphi \gamma)$反映了模型中波动率的记忆性，
称为持久性系数(persistent parameter) 。


### 模拟Realized GARCH(1,1)数据
```{r,echo = TRUE,results='markup'}
rm(list=ls())
set.seed(456)
n=1000
h=numeric(n) #条件方差
r=numeric(n) #收益率
x=numeric(n) #已实现测度方差
lnx=numeric(n)
lnh=numeric(n)
# 设置参数
omega= 0.06
beta= 0.55
gamma=  0.4
xi= -0.18 
phi= 1.04 
sigu= 0.38 
tau1=-0.07
tau2=0.07
z=rnorm(1000,0,1)
u=rnorm(1000,0,sigu)
h[1]= 0.2
```

___
```{r,echo = TRUE,results='markup'}
for (t in 2:1001){
    lnx[t-1]=xi+phi*log(h[t-1])+(tau1*z[t-1]+tau2*((z[t-1])^2-1))+u[t-1] #测量方程
    x[t-1]=exp(lnx[t-1])
    lnh[t]=omega+beta*log(h[t-1])+gamma*log(x[t-1])#方差方程
    h[t]=exp(lnh[t])
    r[t]=sqrt(h[t])*z[t] #收益率方程
}
r[2:10]
x[2:10]
h[2:10]
```

## Realized Wishart GARCH
### Realized Wishart GARCH模型[@周少甫2019;@Hansen2016a]
这是一种新颖的多元GARCH模型，该模型结合了收益方差矩阵的已实现度量。 创新之处在于
针对收益、已实现的方差和已实现的协方差的多元**动态**模型的联合制定。方差矩阵的更
新依赖于基于高斯和Wishart密度的联合似然函数的得分函数。 

这个模型可以看做是Realized GARCH模型在多变量情况下的扩展，但是它是新颖的，因为我
们对时变条件方差矩阵采用了分数驱动方法。联合建模框架**依赖于Wishart密度来实现已
实现方差矩阵，并依赖于高斯密度来实现日收益向量。**时变方差矩阵的更新由预测联合似
然函数的标定分数(scaled score)驱动。分数函数被证明是日收益的外部乘积与实际已实现
度量的加权组合。**加权取决于Wishart分布中的自由度数**。我们将结果模型称为
Wishart-GARCH模型。Wishart-GARCH模型的一个关键特征是
**条件方差矩阵的动态依赖于预测可能性的得分函数**。

### Wishart-GARCH模型构造
   
令$r_t\in R^k$是$k$个资产日对数收益率的$k\times 1$向量，令$X_t\in R^{k\times k}$表
示k个资产对数收益率在时间t的$k\times k$的已实现协方差矩阵，其中$t = 1，\cdots，T。$
令$F_{t-1}$是$r_t$和$X_t$的历史值生成的$\sigma$域。根据以上设定假设以下条件密度：
$$
r_t|F_{t-1}\sim N_k(0,H_t)$$
$$X_t|F_{t-1}\sim W_k(V_t,v)$$

其中$H_t$是均值为0的多元正态分布$N_k(0,H_t)$的$k\times k$方差矩阵(即$r_t$服从均值
为0，协方差矩阵为非奇异矩阵$H_t$的多元正态分布$N_k(0,H_t)$);$X_t$服从均值为$V_t$，自由度为v的k维
Wishart分布，其中$V_t$是非奇异矩阵,$v\geq k$。方差矩阵$H_t$和$V_t$在$F_{t-1}$上可测。
式(9)和式(10)的变量$r_t$和$X_t$是条件独立的。同时$r_t$和$X_t$间的相依性取决于$H_t$ 和$V_t$
间的相依性。参数v描述了$X_t$ 测量$V_t$的精确度，v越大，表示$X_t$ 测量$V_t$越精
确。

___

$r_t|F_{t-1}$的正态分布密度函数为:$$\frac{1}{(2 \pi)^{\frac{k}{2}}|H
t|^{\frac{1}{2}}} \exp \{-\frac{1}{2} \operatorname{tr}\left(H_{t}^{-1} r_{t}
r_{t}^{\prime}\right)\}$$ $X_t|F_{t-1}$的Wishart分布密度函数为
:$$\frac{\left|X_{t}\right|^{(v-k-1) / 2}}{2^{\frac{v t}{2}} v^{-\frac{v
k}{2}}\left|V_{t}\right|^{\frac{v}{2}} \Gamma_{k^{(t)}}} \exp
\left\{-\frac{v}{2} \operatorname{tr}\left(V_{t}^{-1} X_{t}\right)\right\}$$

其中Gamma函数$\Gamma_{k}(a)=\pi^{\frac{k(k-1)}{4}} \prod_{i=1}^{k}\Gamma(a+(1-i)
/ 2)$，且 a 为正数。

___

**备注1：**假设已实现协方差$X_t$对所有$t=1,\cdots,T$都能通过Barndorff-Nielsen等
(2011)[@Barndorff2008Multivariate]所描述的多变量已实现核(RK)估计测量。

**备注2：Wishart 分布**

Wishart分布是$\chi^2$分布在多元上的推广。因此，它可以用来描述多元正态分布样本的
协方差矩阵。Wishart分布是一组定义在对称、非负定矩阵的随机变量（随机矩阵）。假设X
是一个$n\times k$的矩阵,每一行都是从均值为0的k维正态分布中抽取的独立样本，即
$X_{(i)}=\left(x_{i}^{1}, \cdots, x_{i}^{k}\right) \sim N_{k}(\mathbf{0},
\Sigma)$.那么，Wishart分布S就是这个$k\times k$的随机矩阵的概率分布：
$S=\sum_{i=1}^{n} X_{i}^{T} X_{i}$,记为$S \sim W_{p}(\Sigma, n)$

___

根据前面的分布假设，日收益率$r_t$的分布应满足$$r_tr_t^{\prime}|F_{t-1}\sim
W_k(H_t,1)$$

$W_k(H_t,1)$是均值为$H_t$，自由度为1的奇异Wishart分布。如果$r_t$是"收盘价-收盘价
"的收益率向量，则$H_t$衡量隔夜方差和日内方差，而$V_t$衡量日内方差。 如果$r_t$是"
开盘价-收盘价"的收益率向量，则$H_t$和$V_t$都将测量特定交易日的方差。 在计算已实
现的度量时，通常会排除隔夜收益，而对日收益的建模则基于"开盘价-收盘价"和"收盘价-
收盘价"。

根据密度函数令**测量方程**为：
$$r_t|F_{t-1}=H_t^{1/2}\varepsilon_t$$
$$X_{t}|F_{t-1}=V_{t}^{1/2} \eta_{t} V_{t}^{1 / 2}$$


其中，$\varepsilon_t$和$\eta_{t}$分别服从如下分布：$$\varepsilon_t \sim
N_k(0,I_k),\eta_{t} \sim W(I_k/v,v)$$

$\varepsilon_t$是向量，$\eta_{t}$是矩阵随机变量,$E(\varepsilon_t,\eta_{s}^{\prime})=0,t,s=1,\cdots,T$

___

根据$r_tr_t^{\prime}|F_{t-1}\sim W_k(H_t,1)$，可重新定义条件密度为(即$r_t$和$X_t$
还满足)：$$r_{t}r_{t}^{\prime}=H_{t}^{1 / 2} \zeta_{t} H_{t}^{1 / 2},\zeta_{t}
\sim W_{k}\left(I_{k}, 1\right)$$
$$X_{t}= V_{t}^{1 / 2} \eta_{t} V_{t}^{1 / 2},\eta_{t} \sim W_{k}\left(I_{k}/v,
v\right)$$

这样就可以用方差和协方差来表示模型的度量方程。假设日对数收益率的条件协方差矩阵和
已实现协方差矩阵的均值为同一个动态过程。 令$H_{t}$满足如下条件：
$$H_{t}=\Lambda V_{t} \Lambda^{\prime}$$

其中$\Lambda$是一个k×k对角矩阵，
$\Lambda_{ii}=\Lambda_i>0,i=1,\cdots,k$

___

根据以上模型设定，可以得到$r_t|F_{t-1}$和$X_{t}|F_{t-1}$的期望以及
$\operatorname{vec}\left(r_{t} r_{t}^{\prime}\right) |
\mathcal{F}_{t-1},\operatorname{vec}\left(\Lambda^{-1} r_{t} r_{t}^{\prime}
\Lambda^{-1}\right) | \mathcal{F}_{t-1}和 vec(X_t)|F_{t-1}$的方差：

$$
\mathbf{E}\left[r_{t} r_{t}^{\prime} | \mathcal{F}_{t-1}\right]=\Lambda
V_{t} \Lambda^{\prime}, \quad \mathbf{E}\left[X_{t} |
\mathcal{F}_{t-1}\right]=V_{t}
$$

$$
\begin{aligned} \operatorname{Var}\left[v e c\left(r_{t} r_{t}^{\prime}\right) | F_{t-1}\right]=\left(I_{k^{2}}+K_{k}\right)(\Lambda \otimes \Lambda)\left(J_{t} \otimes V_{t}\right)\left(\Lambda^{\prime} \otimes \Lambda\right) \\
\quad \operatorname{Var}\left[\operatorname{vec}\left(\Lambda^{-1} r_{t} r_{t}^{\prime}(\Lambda)^{-1}\right) | F_{t-1}\right]=\left(I_{k^{2}}+K_{k}\right)\left(V_{t} \otimes V_{t}\right) \\
\operatorname{Var}\left[\operatorname{vec}\left(X_{t}\right) | F_{t-1}\right]=v^{-1}\left(I_{k^{2}}+K_{k}\right)\left(V_{t} \otimes V_{t}\right) \end{aligned}
$$

其中$K_k$是$k^2 \times k^2$的计算矩阵[@JR].

___

为了构建$V_t$的动态特性，我们引入了向量$f_t$，它被假定为完全唯一地代表$V_t$(令
$V_t$是时变向量过程$f_t$的函数)，即$V_t=V_t(f_t)$, 并且定义$f_t$为：$f_t
=vech(V_t)$,就是将对角矩阵$V_t$的下三角矩阵向量化为$f_t$。为了确保我们分析中的正
定方差矩阵$V_t$为正，我们可以采用Cholesky分解
$$V_t=C_tC_t^{\prime}$$

时变参数向量定义为
$$f_{t}=\operatorname{vech}\left(C_{t}\right) \quad \Leftrightarrow \quad V_{t}=\operatorname{unvech}\left(f_{t}\right) \operatorname{unvech}\left(f_{t}\right)^{\prime}$$

$f_t$是一个$k^{*}\times 1$的向量且$k^{*}=k(k+1)/2$.

### 得分驱动
模型中的时变参数采用得分驱动的方法进行构建。参考Creal 等人 (2013)
[@Creal2013Generalized]处运用得分驱动方法处理时变参数。

对于变量$r_tr_t^{\prime}$和$X_t$有集$\{r_tr_t^{\prime},X_t\}$，其对数似然函数为：
$$\mathcal{L}(\psi)=\sum_{t=1}^{T}(\mathcal{L}_{r, t}+\mathcal{L}_{X, t})$$

其中：
$$\begin{aligned}
\mathcal{L}_{r, t} &=d_r(k)-\frac{1}{2} \log \left|\Lambda V_{t}
\Lambda\right|-\frac{1}{2} \operatorname{tr}\left(\left(\Lambda
V_{t} \Lambda\right)^{-1} r_{t} r_{t}^{\prime}\right)
\end{aligned}$$

$$\begin{aligned}
\mathcal{L}_{X, t} &=d_X(k, \nu)+\frac{\nu-k-1}{2} \log \left|X_{t}\right|-\frac{\nu}{2} \log \left|V_{t}\right|-\frac{\nu}{2} \operatorname{tr}\left(V_{t}^{-1} X_{t}\right)\end{aligned}$$

___

其中
$d(k)=-\frac{k}{2} \log (2 \pi) , d(k, \nu)=\frac{\nu k}{2} \log (\nu)-\frac{\nu k}{2} \log (2)-\log \left(\Gamma_{k}\left(\frac{\nu}{2}\right)\right),\Gamma_{k}()是k维Gamma函数$



对于时变参数$f_t$，其更新通过递归方程进行：
$$f_{t+1}=\omega+\sum_{i=1}^{p} B_{i} f_{t-i+1}+\sum_{j=1}^{q} A_{j} s_{t-j+1}$$

其中，$\omega$是$d \times 1$的常数向量，$s_t$是零均值有限方差的
鞅差序列，$B_i$和$A_j$是$d \times d$的参数矩阵。参数
$\omega$,$B_1,\cdots,B_p$,$A_1,\cdots,A_q$
和某些可能的密度特定的未知参数（例如Wishart密度的自由度数）都收
集在静态参数向量$\psi$中,对于时变参数的线性更新，难点在于构造$s_t$零均值有限方差
的鞅差序列。

___

这里将$s_t$等于预测似然函数的标度分数，在标准规律性条件下形成一个鞅差
（martingale）序列。得分向量采用下式给出的加法形式,是各个分数的总和：
$$\nabla_{t}=\sum_{i=1}^{m} \nabla_{i, t}=\sum_{i=1}^{m} \frac{\partial \log
\varphi_{i}\left(Z_{t}^{i} | f_{t}, \mathcal{F}_{t-1} ; \psi\right)}{\partial
f_{t}}$$

标度项基于Fisher信息矩阵，也以加法形式表示：$$\mathcal{I}_{t}=\sum_{i=1}^{m}
\mathcal{I}_{i, t}=\sum_{i=1}^{m} \mathrm{E}\left[\nabla_{i, t} \nabla_{i,
t}^{\prime} | \mathcal{F}_{t-1}\right]$$

基于得分向量和Fisher信息矩阵，$$s_t=\mathcal{I}_{t}^{-1}\nabla_{t}$$.

可知$E[s_t|F_t]=0,E[s_ts_t^{\prime}|F_t]=I_d$

对于更新方程，需要获得得分向量和Fisher信息矩阵。

时变参数$f_t$通过一阶递归方程更新为：$$f_{t+1}=\omega+B f_{t}+As_{t}$$

其中，$\omega$是$d \times 1$的常数向量，$s_t$是零均值有限方差的
鞅差序列，$B$和$A$是$d \times 1$的参数矩阵。

___

**定理1：**对于测量密度(14)，(15)以及分解(22)，$k^{*}\times 1$的得分向量为：
$$\begin{aligned} &\nabla_{t}=\frac{1}{2} D_{k}^{\prime}\left(V_{t}^{-1} \otimes
V_{t}^{-1}\right)\left(\nu\left[\operatorname{vec}\left(X_{t}\right)-\operatorname{vec}\left(V_{t}\right)\right]+\left[\operatorname{vec}\left(\Lambda^{-1} r_{t} r_{t}^{\prime} \Lambda^{-1}\right)-\operatorname{vec}\left(V_{t}\right)\right]\right)\\
&\text {其中} D_{k}vech(A)=vec(A) \end{aligned}$$

从定理1可以得出，为确保正定性,基于得分的模型推导对于应用到协方差矩阵$V_t$的分解不
是不变的。 依存关系通过$V_t$输入，该项对于选定的分解是唯一的。 我们发现$V_t$包含
相对于$f_t$的完整协方差矩阵$V_t = V_t（f_t）的一阶导数.

**定理2：**对于测量密度(15)，(16)以及分解(23)，$k^{*}\times k^{*}$的Fisher信息矩阵为
$$\mathcal{I}_{t}=\mathbf{E}\left[\nabla_{t} \nabla_{t}^{\prime} |
\mathcal{F}_{t-1}\right]=\frac{1+\nu}{2}
D_{d}^{\prime}\left(V_{t}^{-1} \otimes
V_{t}^{-1}\right) D_{k}$$

可得一阶递归方程下的得分向量为：
$$s_{t}=J_{t}^{-1} \nabla_{t}=\frac{1}{v+1}\left(\operatorname{\nu vech}\left(X_{t}\right)+\operatorname{vech}\left(\Lambda^{-1} r_{t} r_{t}^{\prime}(\Lambda^{\prime})^{-1}\right)\right)-\operatorname{vech}\left(V_{t}\right)$$

为使模型简单易于估计，假设A、B为对角矩阵，且$A=\alpha I_k^{*},A=\beta I_k^{*}$

### Wishart-GARCH模型形式
综合以上分析，总结出Wishart-GARCH(p,q)的模型形式：

$$\begin{cases}
r_t|F_{t-1}=H_t^{1/2}\varepsilon_t,\varepsilon_t\sim N_k(0,I_k)\\
X_{t}= V_{t}^{1 / 2} \eta_{t} V_{t}^{1 / 2},\eta_{t} \sim W_{k}\left(I_{k}/v,
v\right)\\
f_{t+1}=\omega+\sum_{i=1}^{p} B_{i} f_{t-i+1}+\sum_{j=1}^{q} A_{j} s_{t-j+1}\\
\end{cases}$$

$r_t\in R^k$是$k$个资产日对数收益率的$k\times 1$向量，$X_t\in R^{k\times k}$表示
k个资产对数收益率在时间t的$k\times k$的已实现协方差矩阵，其中$t = 1，\cdots，
T$。$F_{t-1}$是$r_t$和$X_t$的历史值生成的$\sigma$域。$H_t$是均值为0的多元正态分
布$N_k(0,H_t)$的$k\times k$方差矩阵;$V_t$是自由度ν($v\geq k$)的第k维Wishart分布
$W_k(V_t,v)$的均值。为使模型简单易于估计，假设A、B为对角矩阵，且$A=\alpha
I_k^{*},A=\beta I_k^{*}$,$s_t$是零均值有限方差的鞅差序列（得分向量）。

___

Wishart-GARCH(1,1)模型为：
$$ \begin{cases}
 r_t|F_{t-1}=H_t^{1/2}\varepsilon_t,\varepsilon_t\sim N_k(0,I_k) \\
 X_{t}= V_{t}^{1 / 2} \eta_{t} V_{t}^{1 / 2},\eta_{t} \sim W_{k}\left(I_{k}/v,
v\right)\\
 f_{t+1}=\omega+B f_{t}+As_{t}
\end{cases}$$

其中$f_t=vech(V_t)$；$s_{t}=J_{t}^{-1} \nabla_{t}=\frac{1}{v+1}\left(\operatorname{\nu vech}\left(X_{t}\right)+\operatorname{vech}\left(\Lambda^{-1} r_{t} r_{t}^{\prime}(\Lambda^{\prime})^{-1}\right)\right)-\operatorname{vech}\left(V_{t}\right)$；$H_{t}=\Lambda V_{t} \Lambda^{\prime}$，$A=\alpha
I_k^{*},A=\beta I_k^{*}$


### 估计过程(p=1,q=1)
建立好上述模型后，便可以开始进行估计与预测。首
先，需要尝试多种降噪方法，将不同方法得到的已实现协
方差矩阵的估计结果分别代入模型中。然后根据模型设
定进行估计。当然这个模型的迭代需要初始值，因此在这
里预设初始值：

设定模型的静态参数向量为：$\psi=\left[\operatorname{vec}(\Lambda)^{\prime}, \omega^{\prime}, \operatorname{vec}(A)^{\prime}, \operatorname{vec}(B)^{\prime},v\right]^{\prime}$

为估计模型参数，需要先设定初始项。令式(31）的初始项$f_0$为样本数据中已实现协方
差矩阵的平均值再进行向量化的结果，由此可得$\omega_0$[@周少甫2019]。其余模型参数
的初始值参照Gorgi等（2016）[@Hansen2016a]中则设定为：$v=k$，$\alpha=0.3$ ， $\beta=0.95$ ，
$\Lambda_{ij} =1$ ，然后使用拟牛顿法中的BFGS算法估计，使对数似然函数式
$\mathcal{L}(\psi)$最大，得到估计结果。



### 模拟Realized Wishart GARCH(1,1)数据

```{r}
rm(list=ls())
library(Rcpp)
library(RcppArmadillo)
# 选择数据集的维度,模拟5只股票
p=5
# 选择时间序列长度
n=500
# 设置参数值
lambda=1 
w=0.1 
v=p+10 
b=0.97 
a=0.30 
theta=c(v,b,a,lambda,w)   
## 产生误差项
set.seed(123)
eta=rWishart(n, theta[1], diag(1,p)/theta[1])  #Wishart分布（500个5*5的矩阵）  
e=matrix(rnorm(n*p),n,p)    #Normal分布   500行正态分布
```
___

```{r}
   sqrt_mat=function(x){
     n=nrow(x)
     B=matrix(0,n,n)
     A=eigen(x,symmetric=T)
     eigval=A$values
     eigvec=A$vectors
     for (i in 1:n) B[i,i]=sqrt(eigval[i])
   return(eigvec%*%B%*%t(eigvec))
   }
```
___

```{r}
## 构建创造序列的函数
gen_series=function(theta,e,eta) {
   n = nrow(e);p = ncol(e);v = theta[1]
   b = theta[2];a = theta[3];w = theta[5]
   La=diag(p)*theta[4];La1=diag(p)*(1/theta[4]);#逆
   y=matrix(0,n,p) #收益率
   Ik=diag(p) #单位阵
   X=array(0,dim=c(p,p,n)) #已实现(协)方差阵
   V=array(0,dim=c(p,p,n)) # (协)方差矩阵
   H=array(0,dim=c(p,p,n)) #(协)方差矩阵
   s=array(0,dim=c(p,p,n))  #得分向量矩阵
   V[,,1]=w/(1-b)*Ik  #初始参数
   H[,,1]=La%*%V[,,1]%*%La 
   y[1,]=t(sqrt_mat(H[,,1])%*%as.matrix(e[1,]))
   X[,,1]=sqrt_mat(V[,,1])%*%eta[,,1]%*%sqrt_mat(V[,,1])
   for (t in 2:n){
      s[,,t-1]= 1/(v+1)*(v*X[,,t-1]+La1%*%y[t-1,]%*%t(y[t-1,])%*%La1)-V[,,t-1]#得分
      V[,,t]=w*Ik+b*V[,,t-1]+a*s[,,t-1]#测量方程   
      H[,,t]=La%*%V[,,t]%*%La1 
      y[t,]=t(sqrt_mat(H[,,t])%*%as.matrix(e[t,])) #收益率
      X[,,t]=sqrt_mat(V[,,t])%*%eta[,,t]%*%sqrt_mat(V[,,t]) #已实现协方差阵
   }
  return(list(V,y,X))
}
```
___

```{r,echo = TRUE,results='markup'}
gen=gen_series(theta,e,eta)
V=gen[[1]]  # 协方差阵
X=gen[[3]]  #已实现协方差阵
y=gen[[2]]  #收益率序列
V[,,2]
X[,,2]
y[1:3,]
```


## Realized  BETA GARCH
### Realized  BETA GARCH模型

**各参数设置：**

$r_{0, t}$和$x_{0, t}$表示市场收益率和相应的已实现方差测度。$r_{1, t}$和$x_{1,
t}$表示单个资产的收益率和相应的已实现方差测度。$y_{1, t}$表示已实现的相关性测
度，$y_{1, t}\in(-1,1)$,$h_{0, t}=\operatorname{var}\left(r_{0, t} |
\mathcal{F}_{t-1}\right)$,$h_{1, t}=\operatorname{var}\left(r_{1, t} |
\mathcal{F}_{t-1}\right)$,$\mathcal{F}_{t}=\sigma\left(\mathcal{X}_{t}, \mathcal{X}_{t-1}, \ldots\right)
\quad \text { with } \quad \mathcal{X}_{t}=\left(r_{0, t}, r_{1, t}, x_{0, t},
x_{1, t}, y_{1, t}\right)^{\prime}$

定义 $\rho_{1, t}=\operatorname{corr}\left(r_{0, t}, r_{1, t} |
\mathcal{F}_{t-1}\right),$ ，$\rho_{1, t}$与$\beta_{1, t}$的关系是$\beta_{1,
t}=\rho_{1, t} \sqrt{h_{1, t} / h_{0, t}}$，其中
$$\beta_{1, t}=\operatorname{cov}\left(r_{1, t}, r_{0, t} | \mathcal{F}_{t-1}\right) / \operatorname{var}\left(r_{0, t} | \mathcal{F}_{t-1}\right)$$

这一等式将动态CAPM和$\beta_{1, t}$的动态特性建立了连接。

模型的结构将利用条件密度的简单分解
$$
f\left(r_{0, t}, x_{0, t}, r_{1, t}, x_{1, t}, y_{1, t} | \mathcal{F}_{t-1}\right)=f\left(r_{0, t}, x_{0, t} | \mathcal{F}_{t-1}\right) f\left(r_{1, t}, x_{1, t}, y_{1, t} | r_{0, t}, x_{0, t}, \mathcal{F}_{t-1}\right)
$$

___

首先对市场收益率构建Realized EGARCH模型:
$$\begin{aligned}
r_{0, t} &=\mu_{0}+\sqrt{h_{0, t}} z_{0, t} \\
\log h_{0, t} &=a_{0}+b_{0} \log h_{0, t-1}+c_{0} \log x_{0, t-1}+\tau_{0}\left(z_{0, t-1}\right) \\
\log x_{0, t} &=\xi_{0}+\varphi_{0} \log h_{0, t}+\delta_{0}\left(z_{0, t}\right)+u_{0, t}
\end{aligned}$$

其中$z_{0, t} \sim$ i.i.d. $N(0,1)$ 和 $u_{0, t} \sim$ i.i.d. $N\left(0, \sigma_{u_{0}}^{2}\right)$,$\tau(z)=\tau_{1} z+\tau_{2}\left(z^{2}-1\right),\delta(z)=\delta_{1} z+\delta_{2}\left(z^{2}-1\right)$

___

单个资产的条件收益模型：
$$\begin{aligned}
r_{1, t}&=\mu_{1}+\sqrt{h_{1, t}} z_{1, t}\\
\rho_{1, t}&=\operatorname{cov}\left(z_{0, t}, z_{1, t} |
\mathcal{F}_{t-1}\right)\\
z_{1, t}&=\rho_{1, t} z_{0, t}+\sqrt{1-\rho_{1, t}^{2}} w_{1, t}\\
\log h_{1, t}&=a_{1}+b_{1} \log h_{1, t-1}+c_{1} \log x_{1, t-1}+d_{1} \log
h_{0, t}+\tau_{1}\left(z_{1, t-1}\right)\\
\log x_{1, t}&=\xi_{1}+\varphi_{1} \log h_{1, t}+\delta_{1}\left(z_{1, t}\right)+u_{1, t} \\
F\left(y_{1, t}\right)&=\xi_{10}+\varphi_{10} F\left(\rho_{1, t}\right)+v_{1, t}
\end{aligned}$$

$w_{1, t}=\left(z_{1, t}-\rho_{1, t} z_{0, t}\right) / \sqrt{1-\rho_{1, t}^{2}}$是均值为0，方差为1，且与$ z_{0, t}$不相关

对于多个资产，首先通过市场数据$r_{0, t},x_{0, t}$估计模型，然后分别为
$i=1,2,\cdots,N$，分别估计$r_{i, t},x_{i, t},y_{i, t}$,通过N个资产的条件方差和相
关性来构建NxN的条件协方差矩阵

# 模型例子实现

### Realized wishart garch
对10只股票从2010-01-05到2010-01-29 共18个交易日数据进行建模，数据频率为5min

### 导入数据
```{r,echo = TRUE,results='markup'}
rm(list = ls())
library(Rcpp)
library(RcppArmadillo)
library(highfrequency)  #（无法安装）
library(xts)
#data("sample_5minprices") #10只股票的5min price
load('./proposal/data/sample_5minprices.rda')
head(sample_5minprices)
```

### 计算收益率

```{r,echo = TRUE,results='markup'}
##计算高频对数收益率
dat=as.xts(apply(sample_5minprices,2,log)) #price取对数
data_daily<- split(dat, f = "days") #形成5min日对数数据list
t=length(data_daily) #交易日

rt=list()
for (i in 1:t){
  rt[[i]]=diff(data_daily[[i]])
} 

rt=lapply(rt, na.omit) #删除缺失值，rt为高频对数收益率

```

___

```{r,echo = TRUE,results='markup'}
##计算日对数收益率
ydayprice=matrix(0,nrow = t,ncol = 10) #日股价
for (i in 1:10) {
  ydayprice[,i]= aggregatets(sample_5minprices[,i],on="days",k=1)
} 
y=apply(log(ydayprice),2,diff) #日对数收益率
n=nrow(y) 
rt[[1]]=NULL #为保持日期同步删除高频数据中第一天收益率
y[1]
```
### 估计已实现协方差阵

```{r,echo = TRUE,results='markup'}
#?rKernelCov #使用核估计器实现协方差计算。
rck=list()
for (i in 2:t){
  rck[[i-1]]=rKernelCov(rdata = data_daily[[i]], align.by = "minutes",
                      align.period = 5, makeReturns = FALSE)
}  #按日得到协方差阵列表(基于已实现核估计进行降噪处理)
vec= as.vector(unlist(rck)) 
rvk=array(vec,dim=c(10,10,n)) #协方差阵列表转为协方差阵array
```

### 模型估计

```{r,echo = TRUE,results='markup'}
p=10 #数据集维度
sourceCpp("../Sample code Wishart-GARCH/Wishart.cpp")
# 设置初始参数以进行数值优化
lambda=rep(1,p)
v=p+10
b=0.95
a=0.3
theta_ini=c(v,b,a,lambda)
```
___
```{r,echo = TRUE,results='markup'}
# 数值优化和标准误差
est=optim(par=theta_ini, fn=function(x) llik_c(x,y,rvk)$llik, method="BFGS")
est
```
___
```{r,echo = TRUE,results='markup'}
se=sqrt(diag(solve(optimHess(est$par, fn=function(x) llik_c(x,y,rvk)$llik))))
se
```
___

```{r,echo = TRUE,results='markup'}
## 输出估计结果
print("parameter estimates are:")
round(est$par,3)
print("standard errors are estimates are:")
round(se,3)
```
___

```{r,echo = TRUE,results='markup'}
# 获取时变协方差矩阵
Vt=llik_c(est$par,y,rvk)$f
Vt[,,1]
```
### 损失函数
```{r,echo = TRUE,results='markup'}
Xt=rvk #观测到的已实现协方差矩阵
Vt=llik_c(est$par,y,rvk)$f #由模型预测出的已实现协方差矩阵

#计算损失函数(均方根误差（RMSE）)
RMSE=vector()
for (i in 1:n) {
  RMSE[i]=sqrt(sum((Xt[,,i]-Vt[,,i])^2))
}
RMSE
```
___
```{r,echo = TRUE,results='markup'}
plot(RMSE,type="l")
```

# 参考文献
[//]: # (\bibliography{Bibfile})
